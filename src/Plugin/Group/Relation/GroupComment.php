<?php

namespace Drupal\group_comment_permissions\Plugin\Group\Relation;

use Drupal\group\Plugin\Group\Relation\GroupRelationBase;

/**
 * Provides a content enabler for comments.
 *
 * @GroupRelationType(
 *   id = "group_comment",
 *   label = @Translation("Comments on group entities"),
 *   description = @Translation("Handle group permissions for comments."),
 *   entity_type_id = "comment",
 *   entity_access = TRUE,
 *   enforced = FALSE,
 *   deriver =
 *   "Drupal\group_comment_permissions\Plugin\Group\Relation\GroupCommentDeriver"
 * )
 */
class GroupComment extends GroupRelationBase {

  /**
   * {@inheritDoc}
   */
  protected function getGroupContentPermissions() {
    // The relationship between a comment and a group is computed.
    return [];
  }

  /**
   * {@inheritDoc}
   */
  protected function getTargetEntityPermissions() {
    $permissions = parent::getTargetEntityPermissions();
    $plugin_id = $this->getPluginId();
    // We do not handle per group type 'access comments' and 'post comments'
    // permissions, because:
    // - it's hard to do, because of the comment module implementation
    // - it's usually useless as you cannot comment nor view comments on
    // entities you cannot view.
    unset($permissions["create $plugin_id entity"], $permissions["view $plugin_id entity"]);
    return $permissions;
  }

}
