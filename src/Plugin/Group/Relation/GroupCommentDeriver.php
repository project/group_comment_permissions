<?php

namespace Drupal\group_comment_permissions\Plugin\Group\Relation;

use Drupal\comment\Entity\CommentType;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Plugin derivative.
 */
class GroupCommentDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (CommentType::loadMultiple() as $name => $commentType) {
      $label = $commentType->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group comment (@bundle)', [
        '@bundle' => $label,
      ]));
      $this->derivatives[$name]->set('description', $this->t('Manage permissions on @bundle.', [
        '@bundle' => $label,
      ]));
    }

    return $this->derivatives;
  }

}
