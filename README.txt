CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provide comment permissions based on group roles.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/group_comment_permissions).

REQUIREMENTS
------------

This module requires comment and group modules outside of Drupal core.


INSTALLATION
------------

 * Install the Group comment permissions module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

There is no configuration.


MAINTAINERS
-----------

Current maintainers:

 * Gaël Gosset (GaëlG) - https://www.drupal.org/u/ga%C3%ABlg
